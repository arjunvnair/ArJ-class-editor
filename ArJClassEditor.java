import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rsyntaxtextarea.spell.SpellingParser;

/**
 * 
 * @author Arjun Nair
 * @version 1.0.0
 */
public class ArJClassEditor
{
	/**
	 * The scroll pane to which the System out, in, and err streams are redirected to
	 */
	public static final JScrollPane consolePane = createConsoleScrollPane();
	private static final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();;
	private RSyntaxTextArea editorPane;
	private SpellingParser spellParser;
	private File root;
	private File source;
	private String fileDot;
	private Class<?> cls;
	/**
	 * Constructs an ArJClassEditor
	 * @param rootDirectoryPath the path to the root directory (ex. "src")
	 * @param sourceFilePath the path of package names from the root to the file (ex. "test/Test.java")
	 * @param fileDot the fully qualified name of the class (ex. "test.Test")
	 */
	public ArJClassEditor(String rootDirectoryPath, String sourceFilePath, String fileDot)
	{
		this.root = new File(rootDirectoryPath);
		this.source = new File(rootDirectoryPath, sourceFilePath);
		this.fileDot = fileDot;
	}
	/**
	 * Constructs an IDE editor panel, preset with the file's code, with which the user can edit the file's code
	 * @param d the dimension of the editor panel
	 * @return the IDE editor panel
	 * @throws IOException if file fails to save, rendering the initial compilation a failure
	 * @throws MalformedURLException if the class fails to load into the ArJClassEditor (leaving it unable to run the compiled class)
	 */
	public JPanel createIDEEditorPanel(Dimension d) throws MalformedURLException, IOException
	{
		JScrollPane editorScrollPane = createEditorScrollPane(readFile());
		editorScrollPane.setPreferredSize(new Dimension((int) d.getWidth(), (int) (2/3 * d.getHeight())));
		consolePane.getViewport().setPreferredSize(new Dimension((int) d.getWidth(), (int) (1/3 * d.getHeight())));
		JPanel editorPanel = new JPanel();
		editorPanel.setLayout(new BoxLayout(editorPanel, BoxLayout.PAGE_AXIS));
		editorPanel.setSize(d);
		editorPanel.add(editorScrollPane);
		editorPanel.add(consolePane);
		compile();
		return editorPanel;
	}
	/**
	 * Constructs an IDE editor panel, whose code is set with a parameter, with which the user can edit the file's code
	 * @param initialCode the code that the the file is set to and shows up initially on the editor pane
	 * @param d the dimension of the editor panel
	 * @return the IDE editor panel
	 * @throws IOException if file fails to save, rendering the initial compilation a failure
	 * @throws MalformedURLException if the class fails to load into the ArJClassEditor (leaving it unable to run the compiled class)
	 */
	public JPanel createIDEEditorPanel(String initialCode, Dimension d) throws MalformedURLException, IOException
	{
		JScrollPane editorScrollPane = createEditorScrollPane(initialCode);
		editorScrollPane.setPreferredSize(new Dimension((int) d.getWidth(), (int) (2/3 * d.getHeight())));
		consolePane.getViewport().setPreferredSize(new Dimension((int) d.getWidth(), (int) (1/3 * d.getHeight())));
		JPanel editorPanel = new JPanel();
		editorPanel.setLayout(new BoxLayout(editorPanel, BoxLayout.PAGE_AXIS));
		editorPanel.setSize(d);
		editorPanel.add(editorScrollPane);
		editorPanel.add(consolePane);
		compile();
		return editorPanel;
	}
	/**
	 * Constructs a file save button according to parameters
	 * @param saveButtonText the text of the save button
	 * @return the constructed button
	 */
	public JButton createSaveButton(String saveButtonText)
	{
		JButton saveButton = new JButton(saveButtonText);
		ActionListener saveButtonListener = new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				try 
				{
					saveFile();
				} 
				catch (IOException e1) {}
			}
		};
		saveButton.addActionListener(saveButtonListener);
		return saveButton;
	}
	/**
	 * Constructs a compile button according to parameters
	 * @param compileButtonText the text of the compile button
	 * @return the constructed button
	 */
	public JButton createCompileButton(String compileButtonText)
	{
		JButton compileButton = new JButton(compileButtonText);
		ActionListener compileButtonListener = new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				try 
				{
					compile();
				} 
				catch (IOException e1) 
				{
					source.getParentFile().mkdirs();
					compiler.run(null, null, null, source.getPath());
					try
					{
						loadClass();
					}
					catch(MalformedURLException e2) {}
				}
			}
		};
		compileButton.addActionListener(compileButtonListener);
		return compileButton;
	}
	/**
	 * Constructs a run button according to parameters
	 * @param runButtonText the text of the run button
	 * @return the constructed button
	 */
	public JButton createRunButton(String runButtonText)
	{
		JButton runButton = new JButton(runButtonText);
		ActionListener runButtonListener = new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				try
				{
					run();
				}
				catch (ReflectiveOperationException e1) 
				{
					System.err.println("No main method could be located.");
				}
				catch (SecurityException e1) {} 
			}
		};
		runButton.addActionListener(runButtonListener);
		return runButton;
	}
	/**
	 * Constructs an editor scroll pane
	 * @param code the set code in the editor pane before the user makes edits
	 * @return the constructed scroll pane
	 */
	public JScrollPane createEditorScrollPane(String code)
	{
		editorPane = new RSyntaxTextArea(code);
		editorPane.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
	    editorPane.setCodeFoldingEnabled(true);
		try 
		{
			spellParser = SpellingParser.createEnglishSpellingParser(new File(ArJClassEditor.class.getProtectionDomain().getCodeSource().getLocation().getPath() + "english_dic.zip"), true);
		} catch (IOException ioe) {}
		if (spellParser != null) 
		{
			try 
			{
				File userDict= File.createTempFile("spellDemo", ".txt");
				spellParser.setUserDictionary(userDict);
			} 
			catch (Exception e) {}
			SwingUtilities.invokeLater(
					new Runnable() 
					{
						@Override
						public void run() 
						{
							editorPane.addParser(spellParser);
						}
					}
			);
		}
		JScrollPane editorScrollPane = new JScrollPane(editorPane, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		return editorScrollPane;
	}
	/**
	 * Refreshes the class that the ArJClassEditor runs with the current .class file
	 * @throws MalformedURLException if the class fails to load into the ArJClassEditor
	 */
	public void loadClass() throws MalformedURLException
	{
		URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] { root.toURI().toURL() });
		try 
		{
			cls = Class.forName(fileDot, true, classLoader);
		} 
		catch (ClassNotFoundException e) {}
	}
	/**
	 * Retrieves the code of the .java file in String form
	 * @return a string representing the code in the .java file
	 * @throws IOException if the file is not found
	 */
	public String readFile()
	{
		try
		{
			return new String(Files.readAllBytes(Paths.get(source.toURI())));
		}
		catch(IOException e)
		{
			return "";
		}
	}
	/**
	 * Saves the code onto the .java file
	 * @throws IOException if the file cannot be properly saved
	 */
	public void saveFile() throws IOException
	{
		Files.createDirectories(source.toPath().getParent());
		Files.write(source.toPath(), editorPane.getText().getBytes(StandardCharsets.UTF_8));
	}
	/**
	 * Compiles the code
	 * @throws IOException if the file fails to save, rendering the compilation unsuccessful
	 * @throws MalformedURLException if the class fails to load into the ArJClassEditor
	 */
	public void compile() throws MalformedURLException, IOException
	{
		saveFile();
		source.getParentFile().mkdirs();
		compiler.run(null, null, null, source.getPath());
		loadClass();
	}
	/**
	 * Runs the main method
	 * @return constructed object* 
	 * @throws SecurityException if a security violation has occurred
	 * @throws ReflectiveOperationException if the main method does not exist in the loaded class
	 */
	public void run() throws ReflectiveOperationException, SecurityException
	{
		Class<?>[] params = {String[].class};
		String[] args = {""};
		Object[] paramsObj = {args};
		runStaticMethod("main", params, paramsObj);
	}
	/**
	 * Constructs an instance of the loaded class with no parameters
	 * @return constructed object
	 * @throws NoSuchMethodException if the method could not be found
	 * @throws IllegalAccessException if the method is inaccessible
	 * @throws InstantiationException if the loaded class is abstract
	 */
	public Object instantiate() throws InstantiationException, IllegalAccessException, NoSuchMethodException
	{
		try
		{
			return cls.newInstance();
		}
	    catch(ExceptionInInitializerError e)
		{
	    	if (e.getCause() instanceof RuntimeException) 
			{
	    		((RuntimeException) e.getCause()).printStackTrace();
			}
			else if (e.getCause() instanceof Error) 
			{
				((Error) e.getCause()).printStackTrace();
			}
	    }
	    return null;
	}
	/**
	 * Constructs an instance of the loaded class with parameters
	 * @return constructed object (null if runtime exception occurs)
	 * @throws SecurityException if a security violation has occurred
	 * @throws NoSuchMethodException if the method could not be found
	 * @throws IllegalAccessException if the method is inaccessible
	 * @throws IllegalArgumentException if inappropriate arguments have been passed into the method
	 * @throws InstantiationException if the loaded class is abstract
	 */
	public Object instantiate(Class<?>[] params, Object[] paramsObj) throws InstantiationException, IllegalArgumentException, IllegalAccessException, NoSuchMethodException, SecurityException
	{
		try
		{
			return cls.getDeclaredConstructor(params).newInstance(paramsObj);
		}
		catch(ExceptionInInitializerError | InvocationTargetException e)
		{
	    	if (e.getCause() instanceof RuntimeException) 
			{
	    		((RuntimeException) e.getCause()).printStackTrace();
			}
			else if (e.getCause() instanceof Error) 
			{
				((Error) e.getCause()).printStackTrace();
			}
	    }
	    return null;
	}
	/**
	 * Invokes an instance method
	 * @param methodName the name of the method
	 * @param params a list of the class types corresponding to the method parameters
	 * @param paramsObj a list of the arguments passed in as parameters
	 * @return the result of dispatching the method
	 * @throws SecurityException if a security violation has occurred
	 * @throws NoSuchMethodException if the method could not be found
	 * @throws IllegalAccessException if the method is inaccessible
	 * @throws IllegalArgumentException if inappropriate arguments have been passed into the method
	 */
	public Object runMethod(String methodName, Class<?> params[], Object paramsObj[], Object instance) throws IllegalArgumentException, IllegalAccessException, NoSuchMethodException, SecurityException
	{
		Method thisMethod = cls.getDeclaredMethod(methodName, params);
	    try
	    {
	    	return thisMethod.invoke(instance, paramsObj);
	    }
	    catch(InvocationTargetException e)
		{
	    	if (e.getCause() instanceof RuntimeException) 
			{
	    		((RuntimeException) e.getCause()).printStackTrace();
			}
			else if (e.getCause() instanceof Error) 
			{
				((Error) e.getCause()).printStackTrace();
			}
	    }
	    return null;
	}
	/**
	 * Invokes a static method
	 * @param methodName the name of the method
	 * @param params a list of the class types corresponding to the method parameters
	 * @param paramsObj a list of the arguments passed in as parameters
	 * @return the result of dispatching the method
	 * @throws SecurityException if a security violation has occurred
	 * @throws NoSuchMethodException if the method could not be found
	 * @throws IllegalAccessException if the method is inaccessible
	 * @throws IllegalArgumentException if inappropriate arguments have been passed into the method
	 */
	public Object runStaticMethod(String methodName, Class<?> params[], Object paramsObj[]) throws IllegalArgumentException, IllegalAccessException, NoSuchMethodException, SecurityException
	{
		 return runMethod(methodName, params, paramsObj, null);
	}
	private static JScrollPane createConsoleScrollPane()
	{
		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		TextAreaOutputStream textAreaOutputStream = new TextAreaOutputStream(textArea);
		PrintStream textAreaPrintStream = new PrintStream(textAreaOutputStream);
		System.setOut(textAreaPrintStream);
		System.setErr(textAreaPrintStream);
		return new JScrollPane(textArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
	}
}
