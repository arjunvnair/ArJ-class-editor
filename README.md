# ARJ Class Editor
An intra program IDE API designed to help programmers integrate a Java programming environment within their programs, whether it be for a programming game or a tutorial.

## Example Code

```
import java.awt.Dimension;
import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main 
{
	private static ArJClassEditor test;
	public static void main(String[] args) throws IOException
	{
		test = new ArJClassEditor("C://java", "test/Test.java", "test.Test");
		JFrame f = new JFrame();
		f.setLayout(new BoxLayout(f.getContentPane(), BoxLayout.PAGE_AXIS));
		f.setSize(new Dimension(480, 960));
		if(test.readFile().equals(""))
			f.add(test.createIDEEditorPanel("package test; \n\npublic class Test \n{ \n	public Test()  \n	{ \n		\n	} \n	public static void main(String[] args) \n	{ \n		System.out.println(\"Hello world!\");\n \n	}\n}", new Dimension(480, 720)));	
		else
			f.add(test.createIDEEditorPanel(f.getSize()));	
		JPanel buttonPanel = new JPanel(new GridLayout(3,1));
		buttonPanel.add(test.createSaveButton("Save"));
		buttonPanel.add(test.createCompileButton("Compile"));
		buttonPanel.add(test.createRunButton("Run"));
		f.add(buttonPanel);
		f.setVisible(true);
	}
}
```

## External Dependencies

In order for the ArJClassEditor to work, you must download the following jars and add them to your program.

https://mvnrepository.com/artifact/com.fifesoft/rsyntaxtextarea/2.6.1
https://mvnrepository.com/artifact/com.fifesoft/spellchecker/1.4.0
https://mvnrepository.com/artifact/org.mozilla/rhino/1.7.7.1

You must also use JDK 1.8.0_101 as your project's JRE, as the program needs to be able to compile on the spot.
